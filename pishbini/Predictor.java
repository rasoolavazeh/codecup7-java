public class Predictor {
    public static int predict(int year) {
        // TODO: Implement
        while (true) {
            if (year % 4 == 2)
                return year;
            year++;
        }
    }

    public static void main(String[] args) {
        System.out.println(predict(2021)); // 2022
        System.out.println(predict(2022)); // 2022
        System.out.println(predict(2023)); // 2026
    }
}