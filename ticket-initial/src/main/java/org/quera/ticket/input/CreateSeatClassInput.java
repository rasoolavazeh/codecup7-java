package org.quera.ticket.input;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CreateSeatClassInput {

    long min_number;
    long max_number;
    long match_id;
    long price;
}
