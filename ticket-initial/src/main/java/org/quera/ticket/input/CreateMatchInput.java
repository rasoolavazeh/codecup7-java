package org.quera.ticket.input;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CreateMatchInput {

    long home_id;
    long away_id;
    long stadium_id;
    String date;
}
