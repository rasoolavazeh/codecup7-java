package org.quera.ticket;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.quera.ticket.input.*;
import org.quera.ticket.models.*;
import org.quera.ticket.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;


@RestController
public class Controller {

    @Autowired
    StadiumService stadiumService;

    @Autowired
    TicketService ticketService;

    @Autowired
    TeamService teamService;

    @Autowired
    MatchService matchService;

    @Autowired
    SeatClassService seatClassService;

    @Autowired
    UserService userService;

    @GetMapping(value = "/api/ping", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> ping() {
        return ResponseEntity.ok(new Message("ok"));
    }

    @PreAuthorize(value = "hasRole('ROLE_ADMIN')")
    @PostMapping(value = "/api/users/{id}/update_balance", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> updateBalance(@PathVariable("id") long userId, @RequestBody UpdateBalanceInput input) {
        if (input.getBalance() < 0)
            return ResponseEntity.status(400).body(new Message("Error: invalid balance"));
        if (userService.getUser(userId) == null)
            return ResponseEntity.status(404).body(new Message("Error: user not found"));
        return ResponseEntity.ok(userService.update(userId, input.getBalance()));
    }

    @GetMapping(value = "/api/stadiums", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getStadiums() {
        return ResponseEntity.ok(stadiumService.getStadiums());
    }

    @PreAuthorize(value = "hasRole('ROLE_ADMIN')")
    @PostMapping(value = "/api/stadiums", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> saveStadium(@RequestBody CreateStadiumInput input) {
        Stadium stadium = Stadium.builder()
                .capacity(input.getCapacity())
                .name(input.getName())
                .build();
        Stadium save = stadiumService.save(stadium);
        if (save == null)
            return ResponseEntity.status(400).build();
        return ResponseEntity.ok(save);
    }

    @GetMapping(value = "/api/stadiums/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getStadium(@PathVariable("id") long id) {
        Stadium stadium = stadiumService.get(id);
        if (stadium == null)
            return ResponseEntity.status(404).body(new Message("Error: stadium not found"));
        return ResponseEntity.ok(stadium);
    }

    @PreAuthorize(value = "hasRole('ROLE_ADMIN')")
    @DeleteMapping(value = "/api/stadiums/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> removeStadium(@PathVariable("id") long id) {
        Stadium stadium = stadiumService.get(id);
        if (stadium == null)
            return ResponseEntity.status(404).body(new Message("Error: stadium not found"));
        stadiumService.remove(id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping(value = "/api/teams", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getTeams() {
        return ResponseEntity.ok(teamService.getTeams());
    }

    @PreAuthorize(value = "hasRole('ROLE_ADMIN')")
    @PostMapping(value = "/api/teams", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> saveTeam(@RequestBody Team team) {
        Team save = teamService.save(team);
        if (save == null)
            return ResponseEntity.status(400).build();
        return ResponseEntity.ok(team);
    }

    @GetMapping(value = "/api/teams/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getTeam(@PathVariable("id") long id) {
        Team team = teamService.get(id);
        if (team == null)
            return ResponseEntity.status(404).body(new Message("Error: team not found"));
        return ResponseEntity.ok(team);
    }

    @PreAuthorize(value = "hasRole('ROLE_ADMIN')")
    @DeleteMapping(value = "/api/teams/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> removeTeam(@PathVariable("id") long id) {
        Team team = teamService.get(id);
        if (team == null)
            return ResponseEntity.status(404).body(new Message("Error: team not found"));
        teamService.remove(id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping(value = "/api/matches", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getMatches() {
        return ResponseEntity.ok(matchService.getMatches());
    }

    @PreAuthorize(value = "hasRole('ROLE_ADMIN')")
    @PostMapping(value = "/api/matches", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> saveMatch(@RequestBody CreateMatchInput input) {
        String[] tmp = input.getDate().split("-");
        LocalDate localDate = LocalDate.of(Integer.parseInt(tmp[0]), Integer.parseInt(tmp[1]), Integer.parseInt(tmp[2]));
        if (localDate.isBefore(LocalDate.now()))
            return ResponseEntity.status(400).body(new Message("Error: the given date has been passed"));
        if (teamService.get(input.getHome_id()) == null)
            return ResponseEntity.status(400).body(new Message("Error: invalid home team id"));
        if (teamService.get(input.getAway_id()) == null)
            return ResponseEntity.status(400).body(new Message("Error: invalid away team id"));
        if (stadiumService.get(input.getStadium_id()) == null)
            return ResponseEntity.status(400).body(new Message("Error: invalid stadium id"));
        List<Match> matches = matchService.findByStadiumAndDate(input.getStadium_id(), Date.from(localDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()));
        if (!matches.isEmpty())
            return ResponseEntity.status(400).body(new Message("Error: stadium is full in the given date"));

        Match match = Match.builder()
                .home(teamService.get(input.getHome_id()))
                .away(teamService.get(input.getAway_id()))
                .date(Date.from(localDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()))
                .stadium(stadiumService.get(input.getStadium_id()))
                .build();
        return ResponseEntity.ok(matchService.save(match));
    }

    @GetMapping(value = "/api/matches/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getMatch(@PathVariable("id") long id) {
        Match match = matchService.get(id);
        if (match == null)
            return ResponseEntity.status(404).body(new Message("Error: match not found"));
        return ResponseEntity.ok(match);
    }

    @PreAuthorize(value = "hasRole('ROLE_ADMIN')")
    @DeleteMapping(value = "/api/matches/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> removeMatch(@PathVariable("id") long id) {
        Match match = matchService.get(id);
        if (match == null)
            return ResponseEntity.status(404).body(new Message("Error: match not found"));
        matchService.remove(id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping(value = "/api/seat_classes", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getSeats() {
        return ResponseEntity.ok(seatClassService.getSeatClasses());
    }

    @PreAuthorize(value = "hasRole('ROLE_ADMIN')")
    @PostMapping(value = "/api/seat_classes", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> saveSeat(@RequestBody CreateSeatClassInput input) {

        if (matchService.get(input.getMatch_id()) == null)
            return ResponseEntity.status(400).body(new Message("Error: invalid match id"));
        if (input.getMin_number() < 1)
            return ResponseEntity.status(400).body(new Message("Error: invalid min number"));
        if (input.getMax_number() < input.getMin_number() || input.getMax_number() > stadiumService.get(matchService.get(input.getMatch_id()).getStadium().getId()).getCapacity())
            return ResponseEntity.status(400).body(new Message("Error: invalid max number"));
        if (input.getPrice() < 0)
            return ResponseEntity.status(400).body(new Message("Error: invalid price"));
        if (!seatClassService.canCreate(input.getMatch_id(), input.getMin_number(), input.getMax_number()))
            return ResponseEntity.status(400).body(new Message("Error: overlapping seat class exists"));

        SeatClass seatClass = SeatClass.builder()
                .match(matchService.get(input.getMatch_id()))
                .minNumber(input.getMin_number())
                .maxNumber(input.getMax_number())
                .price(BigDecimal.valueOf(input.getPrice()))
                .build();
        return ResponseEntity.ok(seatClassService.save(seatClass));
    }

    @GetMapping(value = "/api/seat_classes/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getSeat(@PathVariable("id") long id) {
        SeatClass seatClass = seatClassService.get(id);
        if (seatClass == null)
            return ResponseEntity.status(404).build();
        return ResponseEntity.ok(seatClass);
    }

    @PreAuthorize(value = "hasRole('ROLE_ADMIN')")
    @DeleteMapping(value = "/api/seat_classes/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> removeSeat(@PathVariable("id") long id) {
        SeatClass seatClass = seatClassService.get(id);
        if (seatClass == null)
            return ResponseEntity.status(404).build();
        seatClassService.remove(id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping(value = "/api/tickets", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getTickets(Authentication authentication) {
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        String username = userDetails.getUsername();

        return ResponseEntity.ok(ticketService.getTickets(userService.getByUsername(username).getId()));
    }

    @PostMapping(value = "/api/tickets", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> buyTicket(@RequestBody BuyTicketInput input, Authentication authentication) {
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        String username = userDetails.getUsername();
        BigDecimal userBalance = userService.getBalance(username);
        Match match = matchService.get(input.getMatch_id());
        if (match == null)
            return ResponseEntity.status(400).body(new Message("Error: invalid match id"));
        if (match.getDate().before(new Date()))
            return ResponseEntity.status(400).body(new Message("Error: the match has been finished"));
        if (input.getSeat_number() < 1 || input.getSeat_number() > match.getStadium().getCapacity())
            return ResponseEntity.status(400).body(new Message("Error: invalid seat number"));
        if (!seatClassService.isOk(input.getSeat_number()))
            return ResponseEntity.status(400).body(new Message("Error: the seat is not available"));
        if (!ticketService.isOkForUser(userService.getByUsername(username).getId(), input.getSeat_number()))
            return ResponseEntity.status(400).body(new Message("Error: you have already reserved this seat"));
        SeatClass tmp = ticketService.isAvailable(input.getSeat_number());
        if (tmp != null)
            return ResponseEntity.status(400).body(new Message("Error: the seat is reserved by another user"));
        SeatClass seatClass = seatClassService.getByMatchIdAndSeatNumber(input.getMatch_id(), input.getSeat_number());
        if (userBalance != null && userBalance.doubleValue() < seatClass.getPrice().doubleValue())
            return ResponseEntity.status(400).body(new Message("Error: not enough balance"));

        Ticket ticket = Ticket.builder()
                .seatClass(seatClass)
                .user(userService.updateBalanceByUsername(username, seatClass.getPrice()))
                .seatNumber(input.getSeat_number())
                .build();
        return ResponseEntity.ok(ticketService.save(ticket));
    }

    @Getter
    @Setter
    @AllArgsConstructor
    static class Message {
        String message;
    }
}
