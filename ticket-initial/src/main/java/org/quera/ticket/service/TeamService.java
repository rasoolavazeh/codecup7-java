package org.quera.ticket.service;

import org.quera.ticket.models.Match;
import org.quera.ticket.models.Team;
import org.quera.ticket.repository.MatchRepository;
import org.quera.ticket.repository.TeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TeamService {

    @Autowired
    TeamRepository repository;

    public Team save(Team team) {
        Team tmp = repository.findByName(team.getName());
        if (tmp != null)
            return null;
        return repository.save(team);
    }

    public Team get(long id) {
        return repository.findById(id).orElse(null);
    }

    public List<Team> getTeams() {
        return repository.findAll();
    }

    public void remove(long teamId) {
        repository.deleteById(teamId);
    }

    public Team deleteMatch(long teamId, Match match) {
        Team team = get(teamId);
        if (team == null)
            return null;
        team.getHomeMatches().remove(match);
        team.getAwayMatches().remove(match);
        return repository.save(team);
    }
}
