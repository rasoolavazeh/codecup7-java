package org.quera.ticket.service;

import org.quera.ticket.models.SeatClass;
import org.quera.ticket.repository.SeatClassRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SeatClassService {

    @Autowired
    SeatClassRepository repository;

    public List<SeatClass> getSeatClasses() {
        return repository.findAll();
    }

    public SeatClass save(SeatClass seatClass) {
        return repository.save(seatClass);
    }

    public void remove(long seatClassId) {
        repository.deleteById(seatClassId);
    }

    public SeatClass get(long id) {
        return repository.findById(id).orElse(null);
    }

    public SeatClass getByMatchIdAndSeatNumber(long matchId, long seatNumber) {
        List<SeatClass> seatClasses = repository.findByMatchId(matchId);
        if (seatClasses == null)
            return null;
        for (SeatClass seatClass : seatClasses)
            if (seatNumber >= seatClass.getMinNumber() && seatNumber <= seatClass.getMaxNumber())
                return seatClass;
        return null;
    }

    public boolean canCreate(long matchId, long minNumber, long maxNumber) {
        List<SeatClass> seatClasses = repository.findByMatchId(matchId);
        if (seatClasses == null)
            return false;
        for (SeatClass seatClass : seatClasses)
            if ((minNumber >= seatClass.getMinNumber() && minNumber <= seatClass.getMaxNumber())
                    || (maxNumber >= seatClass.getMinNumber() && maxNumber <= seatClass.getMaxNumber()))
                return false;
        return true;
    }

    public boolean isOk(long seatNumber) {
        List<SeatClass> seatClasses = getSeatClasses();
        for (SeatClass seatClass : seatClasses)
            if (seatNumber <= seatClass.getMaxNumber() && seatNumber >= seatClass.getMinNumber())
                return true;
        return false;
    }
}
