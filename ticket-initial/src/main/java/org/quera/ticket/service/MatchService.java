package org.quera.ticket.service;

import org.quera.ticket.models.Match;
import org.quera.ticket.repository.MatchRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class MatchService {

    @Autowired
    MatchRepository repository;

    public List<Match> getMatches() {
        return repository.findAll();
    }

    public Match save(Match match) {
        return repository.save(match);
    }

    public Match get(long id) {
        return repository.findById(id).orElse(null);
    }

    public void remove(long matchId) {
        repository.deleteById(matchId);
    }

    public List<Match> findByStadiumAndDate(long stadiumId, Date date) {
        return repository.findByStadiumIdAndDate(stadiumId, date);
    }
}