package org.quera.ticket.service;

import org.quera.ticket.models.Ticket;
import org.quera.ticket.models.User;
import org.quera.ticket.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

@Service
public class UserService {

    @Autowired
    UserRepository repository;

    public List<User> getUsers() {
        return repository.findAll();
    }

    public User getUser(long id) {
        return repository.findById(id).orElse(null);
    }

    public User update(long userId, double balance) {
        User user = repository.findById(userId).orElse(null);
        if (user == null)
            return null;
        user.setBalance(BigDecimal.valueOf(balance));
        return repository.save(user);
    }

    public User getByUsername(String username) {
        return repository.findByUsername(username).orElse(null);
    }

    public BigDecimal getBalance(String username) {
        User user = getByUsername(username);
        if (user == null)
            return null;
        return user.getBalance();
    }

    public User updateBalanceByUsername(String username, BigDecimal price) {
        User user = getByUsername(username);
        BigDecimal newBalance = BigDecimal.valueOf(user.getBalance().doubleValue() - price.doubleValue());
        user.setBalance(newBalance);
        return repository.save(user);
    }

}
