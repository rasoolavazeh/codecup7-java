package org.quera.ticket.service;

import org.quera.ticket.models.SeatClass;
import org.quera.ticket.models.Ticket;
import org.quera.ticket.repository.TicketRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TicketService {

    @Autowired
    TicketRepository repository;

    public List<Ticket> getTickets(long userId) {
        return repository.findByUserId(userId);
    }

    public List<Ticket> getAllTickets() {
        return repository.findAll();
    }

    public List<Ticket> getBySeatNumber(long seatNumber) {
        return repository.findBySeatNumber(seatNumber);
    }

    public Ticket save(Ticket ticket) {
        return repository.save(ticket);
    }

    public SeatClass isAvailable(long seatNumber) {
        List<Ticket> tickets = getBySeatNumber(seatNumber);
        if (tickets == null || tickets.isEmpty())
            return null;
        for (Ticket ticket : tickets)
            if (ticket.getSeatNumber() == seatNumber)
                return ticket.getSeatClass();
        return null;
    }

    public boolean isOkForUser(long userId, long seatNumber) {
        List<Ticket> tickets = getTickets(userId);
        for (Ticket ticket : tickets)
            if (ticket.getSeatNumber() == seatNumber)
                return false;
        return true;
    }

}
