package org.quera.ticket.service;

import org.quera.ticket.models.Stadium;
import org.quera.ticket.repository.StadiumRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StadiumService {

    @Autowired
    StadiumRepository repository;

    public List<Stadium> getStadiums() {
        return repository.findAll();
    }

    public Stadium save(Stadium stadium) {
        Stadium tmp = repository.findByName(stadium.getName()).orElse(null);
        if (tmp != null)
            return null;
        return repository.save(stadium);
    }

    public Stadium get(long id) {
        return repository.findById(id).orElse(null);
    }

    public void remove(long stadiumId) {
        repository.deleteById(stadiumId);
    }
}
