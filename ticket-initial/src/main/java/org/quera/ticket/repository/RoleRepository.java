package org.quera.ticket.repository;

import org.quera.ticket.models.ERole;
import org.quera.ticket.models.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {

    Set<Role> findByName(ERole name);
}
