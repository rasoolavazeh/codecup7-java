package org.quera.ticket.repository;

import org.quera.ticket.models.SeatClass;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface SeatClassRepository extends JpaRepository<SeatClass, Long> {

    List<SeatClass> findByMatchId(long matchId);
}
