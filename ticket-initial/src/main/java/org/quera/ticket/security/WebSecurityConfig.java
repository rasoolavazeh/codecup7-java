package org.quera.ticket.security;

import org.quera.ticket.models.ERole;
import org.quera.ticket.models.Role;
import org.quera.ticket.models.User;
import org.quera.ticket.repository.RoleRepository;
import org.quera.ticket.repository.UserRepository;
import org.quera.ticket.security.jwt.AuthEntryPointJwt;
import org.quera.ticket.security.jwt.AuthTokenFilter;
import org.quera.ticket.security.services.UserDetailsImpl;
import org.quera.ticket.security.services.UserDetailsServiceImpl;
import org.quera.ticket.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig {
    @Autowired
    UserDetailsServiceImpl userDetailsService;

    @Autowired
    private AuthEntryPointJwt unauthorizedHandler;

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Bean
    public AuthTokenFilter authenticationJwtTokenFilter() {
        return new AuthTokenFilter();
    }

    @Bean
    public DaoAuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();

        authProvider.setUserDetailsService(userDetailsService);
        authProvider.setPasswordEncoder(passwordEncoder());


        return authProvider;
    }

    @Bean
    public AuthenticationManager authenticationManager(AuthenticationConfiguration authConfig) throws Exception {
        return authConfig.getAuthenticationManager();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {

        http.cors().and().csrf().disable()
                .exceptionHandling().authenticationEntryPoint(unauthorizedHandler).and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
                .authorizeRequests().anyRequest().permitAll();

        http.authenticationProvider(authenticationProvider());

        http.addFilterBefore(authenticationJwtTokenFilter(), UsernamePasswordAuthenticationFilter.class);

        return http.build();
    }

    @Bean
    public UserDetailsService userDetailsService() {
        addRoles();
        User admin = User.builder()
                .username("admin")
                .password("admin")
                .balance(BigDecimal.ZERO)
                .roles(roleRepository.findByName(ERole.ROLE_ADMIN))
                .build();
        userRepository.save(admin);
        User normal = User.builder()
                .username("quera")
                .password("12345678")
                .balance(BigDecimal.ZERO)
                .roles(roleRepository.findByName(ERole.ROLE_USER))
                .build();
        userRepository.save(normal);

        return new UserDetailsServiceImpl(userRepository);
    }

    private void addRoles() {
        roleRepository.save(Role.builder()
                .name(ERole.ROLE_ADMIN)
                .build());
        roleRepository.save(Role.builder()
                .name(ERole.ROLE_USER)
                .build());
    }
}
